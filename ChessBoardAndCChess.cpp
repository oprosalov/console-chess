//#include "ChessGame.h"
#include "ChessBoardAndCChess.h"
#include <cstdlib>
#include <algorithm>

#define BOARD_R 33              // Output string rows and columns. Mandatory conditions:                                            // Things to remember:
#define BOARD_K 41              // 1. Must add 9 to "actual" row and column values for borders and dividing lines.                  // 1. string board in CBoard lists rows increasing going down
                                // 2. (BOARD_R - 9) and (BOARD_K - 9) must both be divisible by 8.                                  //    and columns increasing going right (i.e. quadrant IV).
                                // 3. ((BOARD_R - 9) / (BOARD_K - 9)) must equal 3/4.                                               //    pieceOnSquare uses a more user friendly way, with rows
                                                                                                                                    //    increasing going up and columns increasing going right (quadrant I).
                                                                                                                                    // Edit 4/9/2014 : wtf?



// CBoard
CBoard::CBoard()
{
	// Initialize takenPieces arrays.
	for (int i = 0; i < 16; i++) {
			takenPiecesW[i] = 0;
			takenPiecesB[i] = 0;
	}

    // Initialize all squares to 0:
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++)
            pieceOnSquare[i][j] = 0;
    }

    // Black pieces:
    for (int j = 0; j < 8; j++)
        pieceOnSquare[6][j] = new CPawn('B');
    pieceOnSquare[7][0] = new CRook('B');
    pieceOnSquare[7][1] = new CKnight('B');
    pieceOnSquare[7][2] = new CBishop('B');
    pieceOnSquare[7][3] = new CQueen('B');
    pieceOnSquare[7][4] = new CKing('B');
    pieceOnSquare[7][5] = new CBishop('B');
    pieceOnSquare[7][6] = new CKnight('B');
    pieceOnSquare[7][7] = new CRook('B');

    // White pieces:
    for (int j = 0; j < 8; j++)
        pieceOnSquare[1][j] = new CPawn('W');
    pieceOnSquare[0][0] = new CRook('W');
    pieceOnSquare[0][1] = new CKnight('W');
    pieceOnSquare[0][2] = new CBishop('W');
    pieceOnSquare[0][3] = new CQueen('W');
    pieceOnSquare[0][4] = new CKing('W');
    pieceOnSquare[0][5] = new CBishop('W');
    pieceOnSquare[0][6] = new CKnight('W');
    pieceOnSquare[0][7] = new CRook('W');
}

CBoard::~CBoard()
{
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            delete pieceOnSquare[i][j];
            pieceOnSquare[i][j] = 0;    // Must do this because pieceOnSquare still contains addresses (I think).
        }
    }
    for (int i = 0; i < 16; i++) {
        delete takenPiecesW[i];
        delete takenPiecesB[i];
        takenPiecesW[i] = 0;
        takenPiecesB[i] = 0;
    }
}

void CBoard::Print()
{
    const int horizLineGap = ((BOARD_R - 9)/8) + 1;
    const int vertLineGap = ((BOARD_K - 9)/8) + 1;

                                                    // Currently: board[33][41].
    string board[BOARD_R][BOARD_K];                 // 8x8 board, each "square" is ((BOARD_R - 9) / 8) * ((BOARD_K - 9) / 8).
                                                    // Array contains each individual character that makes up the board, incl. spaces and lines.
    // Corners:
    board[0][0] = "\xC9";                           // Top left
    board[BOARD_R - 1][0] = "\xC8";                 // Bottom left
    board[0][BOARD_K - 1] = "\xBB";                 // Top right
    board[BOARD_R - 1][BOARD_K - 1] = "\xBC";       // Bottom right

    // Sides:
    for (int i = 1; i < BOARD_R - 1; i++)           // Left
        board[i][0] = "\xBA";
    for (int i = 1; i < BOARD_R - 1; i++)           // Right
        board[i][BOARD_K - 1] = "\xBA";
    for (int j = 1; j < BOARD_K - 1; j++)           // Top
        board[0][j] = "\xCD";
    for (int j = 1; j < BOARD_K - 1; j++)           // Bottom
        board[BOARD_R - 1][j] = "\xCD";

    // Empty Space:                                 // Must be above everything except corners and sides
    for (int i = 1; i < BOARD_R - 1; i++) {         // so it doesn't overwrite other elements in the array.
        for (int j = 1; j < BOARD_K - 1; j++)       // Everything below here replaces these spaces.
            board[i][j] = " ";
    }

    // Dividing lines for squares:
    for (int i = horizLineGap; i < BOARD_R - 1; i += horizLineGap){   // Horizontal.
        for (int j = 1; j < BOARD_K - 1; j++)
            board[i][j] = "-";
    }
    for (int i = 1; i < BOARD_R - 1; i++) {
        for (int j = vertLineGap; j < BOARD_K - 1; j += vertLineGap)  // Vertical.
            board[i][j] = "|";
    }

    // Black squares:
    for (int i = 1; i < BOARD_R - 1; i++) {
        if ((i >= 1 && i <= 3) ||                   // Rows that start with white square.
            (i >= 9 && i <= 11) ||
            (i >= 17 && i <= 19) ||
            (i >= 25 && i <= 27)) {
            for (int j = 6; j < BOARD_K - 1; j++) {
                board[i][j] = "\xB1";
                if (j % 10 == 9)
                    j += 6;
            }
        }
        if ((i >= 5 && i <= 7) ||                   // Rows that start with black square.
            (i >= 13 && i <= 15) ||
            (i >= 21 && i <= 23) ||
            (i >= 29 && i <= 31)) {
            for (int j = 1; j < BOARD_K - 1; j++) {
                board[i][j] = "\xB1";
                if (j % 10 == 4)
                    j += 6;
            }
        }
    }

    // Assign current state of board (pieceOnSquare) to string board in correct spaces.
                                                                        // Need r and k in loop because pieceOnSquare row and column numbering is different than board.
    for (int i = 2, r = 7, k = 0; i < BOARD_R - 1; i += 4) {            // Loops iterate to the first space (of two, one for color, one for piece symbol) in board.
        for (int j = 2; j < BOARD_K - 1; j += 5) {
            if (pieceOnSquare[r][k] != 0) {
                board[i][j] = pieceOnSquare[r][k]->GetColor();          // Piece's color in first space of square
                board[i][j + 1] = pieceOnSquare[r][k]->GetPiece();      // Piece's symbol in second space.
            }
            k++;
        }
        r--;
        k = 0;
    }

    // Print board, row numbers, takenPieces:
    for (int i = 0, rowNum = 8; i < BOARD_R; i++){
        cout << "\t";
        if ((i % 4) == 2) {                           // Row numbers.
            cout << "\b" << rowNum;
            rowNum--;
        }
        for (int j = 0; j < BOARD_K; j++){            // Board string.
            cout << board[i][j];
        }

        // Taken pieces.
        cout << " ";
        switch (i) {
            case 2:
                if (takenPiecesW[0] != 0)
                    cout << "   Black captured:";
                break;
            case 4:
                    for (int t = 0; t < 8; t++) {
                        if (takenPiecesW[t] != 0) {
                            cout << takenPiecesW[t]->GetColor() << takenPiecesW[t]->GetPiece() << " ";
                        }
                    }
                break;
            case 5:
                for (int t = 8; t < 16; t++) {
                if (takenPiecesB[t] != 0)
                    cout << takenPiecesW[t]->GetColor() << takenPiecesW[t]->GetPiece() << " ";
                }
                break;
            case 26:
                if (takenPiecesB[0] != 0)
                    cout << "   White captured:";
                break;
            case 28:
                for (int t = 0; t < 8; t++) {
                    if (takenPiecesB[t] != 0)
                        cout << takenPiecesB[t]->GetColor() << takenPiecesB[t]->GetPiece() << " ";
                }
                break;
            case 29:
                for (int t = 8; t < 16; t++) {
                    if (takenPiecesB[t] != 0)
                        cout << takenPiecesB[t]->GetColor() << takenPiecesB[t]->GetPiece() << " ";
                }
                break;
        }
        cout << endl;                                 // Must have endl here or everything will print on one line.
    }

    cout << "\t";
    for (int j = 0, counter = 1; j < BOARD_K; j++) {               // Column numbers.
        if (j % 5 == 2) {
            cout << counter;
            counter++;
        }
        else
            cout << " ";
    }
    cout << endl << endl;

} // Print()

bool CBoard::IsInCheck(char pieceColor)
{
    // Find the king
    int iKingRow;
    int iKingCol;
    for (int iRow = 0; iRow < 8; ++iRow) {
        for (int iCol = 0; iCol < 8; ++iCol) {
            if (pieceOnSquare[iRow][iCol] != 0) {
                if (pieceOnSquare[iRow][iCol]->GetColor() == pieceColor) {
                    if (pieceOnSquare[iRow][iCol]->GetPiece() == '\xC5') {
                        iKingRow = iRow;
                        iKingCol = iCol;
                    }
                }
            }
        }
    }
    // Run through the opponent's pieces and see if any can take the king
    for (int iRow = 0; iRow < 8; ++iRow) {
        for (int iCol = 0; iCol < 8; ++iCol) {
            if (pieceOnSquare[iRow][iCol] != 0) {
                if (pieceOnSquare[iRow][iCol]->GetColor() != pieceColor) {
                    if (pieceOnSquare[iRow][iCol]->IsLegalMove(iRow, iCol, iKingRow, iKingCol, pieceOnSquare)) {
                        cout << pieceColor << " is in Check!" << endl;
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

bool CBoard::CanMove(char pieceColor)
{
    // Run through all pieces
    for (int iRow = 0; iRow < 8; ++iRow) {
        for (int iCol = 0; iCol < 8; ++iCol) {
            if (pieceOnSquare[iRow][iCol] != 0) {
                // If it is a piece of the current player, see if it has a legal move
                if (pieceOnSquare[iRow][iCol]->GetColor() == pieceColor) {
                    for (int iMoveRow = 0; iMoveRow < 8; ++iMoveRow) {
                        for (int iMoveCol = 0; iMoveCol < 8; ++iMoveCol) {
                            if (pieceOnSquare[iRow][iCol]->IsLegalMove(iRow, iCol, iMoveRow, iMoveCol, pieceOnSquare)) {
                                // Make move and check whether king is in check
                                CPiece* qpTemp					= pieceOnSquare[iMoveRow][iMoveCol];
                                pieceOnSquare[iMoveRow][iMoveCol]	= pieceOnSquare[iRow][iCol];
                                pieceOnSquare[iRow][iCol]			= 0;
                                bool bCanMove = !IsInCheck(pieceColor);
                                // Undo the move
                                pieceOnSquare[iRow][iCol]			= pieceOnSquare[iMoveRow][iMoveCol];
                                pieceOnSquare[iMoveRow][iMoveCol]	= qpTemp;
                                if (bCanMove) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return false;
}


// CChess

CChess::CChess() : playerTurn('W') {}

CChess::~CChess() {}

void CChess::MainMenu(bool wasMenuCalled)
{
    if (wasMenuCalled) {    // If menu is called during a game: Reconstruct pieceOnSquare. Normally I would just use 'new' but I keep getting seg faults if I make CChess game a pointer.
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                delete mqGameBoard.pieceOnSquare[i][j];
                mqGameBoard.pieceOnSquare[i][j] = 0;    // Must do this because pieceOnSquare still contains addresses.
            }
        }
        for (int i = 0; i < 16; i++) {
            delete mqGameBoard.takenPiecesW[i];
            delete mqGameBoard.takenPiecesB[i];
            mqGameBoard.takenPiecesW[i] = 0;
            mqGameBoard.takenPiecesB[i] = 0;
        }
        // Black pieces:
        for (int j = 0; j < 8; j++)
            mqGameBoard.pieceOnSquare[6][j] = new CPawn('B');
        mqGameBoard.pieceOnSquare[7][0] = new CRook('B');
        mqGameBoard.pieceOnSquare[7][1] = new CKnight('B');
        mqGameBoard.pieceOnSquare[7][2] = new CBishop('B');
        mqGameBoard.pieceOnSquare[7][3] = new CQueen('B');
        mqGameBoard.pieceOnSquare[7][4] = new CKing('B');
        mqGameBoard.pieceOnSquare[7][5] = new CBishop('B');
        mqGameBoard.pieceOnSquare[7][6] = new CKnight('B');
        mqGameBoard.pieceOnSquare[7][7] = new CRook('B');

        // White pieces:
        for (int j = 0; j < 8; j++)
            mqGameBoard.pieceOnSquare[1][j] = new CPawn('W');
        mqGameBoard.pieceOnSquare[0][0] = new CRook('W');
        mqGameBoard.pieceOnSquare[0][1] = new CKnight('W');
        mqGameBoard.pieceOnSquare[0][2] = new CBishop('W');
        mqGameBoard.pieceOnSquare[0][3] = new CQueen('W');
        mqGameBoard.pieceOnSquare[0][4] = new CKing('W');
        mqGameBoard.pieceOnSquare[0][5] = new CBishop('W');
        mqGameBoard.pieceOnSquare[0][6] = new CKnight('W');
        mqGameBoard.pieceOnSquare[0][7] = new CRook('W');
    }

    int option;
    cout << "\n\t\tMAIN MENU\n\n"
         << "\t1. Start a single player game.\n"
         << "\t2. Start a two player game. \n"
         << "\t3. Load a game. \n"
         << "\t4. Instructions.\n"
         << "\t5. Exit.\n"
         << "\n\tChoose an option: ";

   // cin >> option;

    while(!(cin >> option)){
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << "\tInvalid input.  Try again: ";
	}


    switch (option) {
        case 1:
        {
            cout << "\n\tNot yet implemented. ETA: 04/01/2047 \n\n\n";
            MainMenu();
            break;
        }

        case 2:
        {
            StartNewGame();
            break;
        }
        case 3:
        {
            StartLoadGame();
            break;
        }
        case 4:
        {
            ShowInstructions();
            MainMenu();
            break;
        }
        case 5:
        {
            exit(0);
            break;
        }
        default:
        {
            cout << "\tInvalid option. Try again.\n\n\n";
            //cin.clear();
            //cin.ignore(std::numeric_limits<std::streamsize>::max());
            MainMenu();
            break;
        }
    }
}

void CChess::StartNewGame()
{
    do {
        GetNextMove(mqGameBoard.pieceOnSquare);
        AlternateTurn();
    } while (!IsGameOver());
    mqGameBoard.Print();
}

void CChess::StartLoadGame()
{
    LoadGame();
    // Assign 8x8 array from saved game (created in LoadGame()) to defaultly constructed pieceOnSquare.
    for (int r = 0; r < 8; r++) {
        for (int k = 0; k < 8; k++)
            mqGameBoard.pieceOnSquare[r][k] = loadPieceOnSquare[r][k];
    }
    do {
        GetNextMove(mqGameBoard.pieceOnSquare);
        AlternateTurn();
    } while (!IsGameOver());
    mqGameBoard.Print();
}

void CChess::ShowInstructions()
{
    cout << "\n\n\t\tINSTRUCTIONS\n\n"
         << "\tMake moves by inputting pieces as a two digit number, RowColumn.\n"
         << "\te.g. Moving white's left most pawn up two squares would look like this: \n"
         << "\tW's Move: 21\n"
         << "\tTo: 41\n\n"
         << "\tYou may also input these commands at any time: save, load, menu, exit\n\n\n";
}

void CChess::GetNextMove(CPiece* pieceOnSquare[8][8])
{
    bool bValidMove = false;
    do {
        mqGameBoard.Print();

        // Input and convert to coordinates:
        /// Maybe make own function.
        string input;
        long int x;             // long because of strtol.
        bool bError;            // This is so that if input is not numbers but also doesn't match the below conditions, the loop restarts.
        bool bRedoInput;        // To continue game after saving.
        do {
            bError = false;
            bRedoInput = false;

            cout << "\t" << playerTurn << "'s Move: ";
			cin >> input;
			transform(input.begin(), input.end(), input.begin(), ::toupper);

            if (IsCommand(input)) {
                if (input == "SAVE") {
                    SaveGame(playerTurn, mqGameBoard.pieceOnSquare);
                    bRedoInput = true;
                }
                else if (input == "MENU") {
                    cout << "\n\n\n\n";
                    MainMenu(true);
                    return;
                }
                else if (input == "EXIT")
                    exit(0);
                else
                    bError = true;  // Need this because currently IsCommand only makes sure there are no digits in the input.
            }
            if (bError)
                cout << endl << "\tError: Please enter either a piece position or a supported command." << endl <<endl;

            // If input is not a command, convert to string int. Can't find how to do strtol with strings,
            // ergo the char conversion.
            else {
				const char* c = input.c_str();
                /*for (int i = 0; i < input.length(); i++)
                    charFromString[i] = input[i];*/

                x = strtol(c, NULL, 0);
            }
        } while(bError || bRedoInput);
        ///

        int iStartRow = (x / 10) - 1;
        int iStartCol = (x % 10) - 1;

        cout << "\tTo: ";
        int iEndMove;
        cin >> iEndMove;
        cout << endl;
        int iEndRow = (iEndMove / 10) - 1;
        int iEndCol = (iEndMove % 10) - 1;

        //cout << "|\t|\t|\t|\t|\t|" << endl;

        // Check that the indices are in range and that the source and destination are different:
        if ((iStartRow >= 0 && iStartRow <= 7) &&
            (iStartCol >= 0 && iStartCol <= 7) &&
            (iEndRow >= 0 && iEndRow <= 7) &&
            (iEndCol >= 0 && iEndCol <= 7)) {
            // Additional checks in here
            CPiece* qpCurrPiece = pieceOnSquare[iStartRow][iStartCol];
            // Check that the piece is the correct color
            if ((qpCurrPiece != 0) && (qpCurrPiece->GetColor() == playerTurn)) {
                // Check that the destination is a valid destination
                if (qpCurrPiece->IsLegalMove(iStartRow, iStartCol, iEndRow, iEndCol, pieceOnSquare)) {
                    // Make the move
                    CPiece* qpTemp = pieceOnSquare[iEndRow][iEndCol];               // Temp save piece at destination
                    pieceOnSquare[iEndRow][iEndCol] = qpCurrPiece;
                    pieceOnSquare[iStartRow][iStartCol]	= 0;

                    // Make sure that the current player is not in check and finalize move.
                    if (!mqGameBoard.IsInCheck(playerTurn)) {
                        bValidMove = true;

                        // Updates bool hasMoved if a piece has made a move.
                        if (pieceOnSquare[iEndRow][iEndCol]->hasMoved == 0)
                            pieceOnSquare[iEndRow][iEndCol]->hasMoved = 1;

                        // Saves taken piece.
                        if (qpTemp != 0 && qpTemp->GetColor() != playerTurn) {
                            if (playerTurn == 'W') {
                                for (int i = 0; i < 16; i++) {                   // Find next available spot in takenPieces.
                                    if (mqGameBoard.takenPiecesB[i] == 0) {
                                        mqGameBoard.takenPiecesB[i] = qpTemp;
                                        //cout << i << " ";
                                        //cout << mqGameBoard.takenPiecesB[i];
                                        //cout << mqGameBoard.takenPiecesB[i]->GetColor() << mqGameBoard.takenPiecesB[i]->GetPiece();
                                        break;
                                    }
                                }
                            }
                            else {
                                for (int i = 0; i < 16; i++) {
                                    if (mqGameBoard.takenPiecesW[i] == 0) {
                                        mqGameBoard.takenPiecesW[i] = qpTemp;
                                        break;
                                    }
                                }
                            }
                        }
                        qpTemp = 0;     // This is a must because the address it points to is copied to takenPieces.
                                        // If you just delete it without this, the piece it points to (now also in takenPieces) is deleted.
                        delete qpTemp;

                        // Move Rook in the case that Castling is performed (King is "legally" moved two spaces left or right)
                        // TODO: this needs to be moved elsewhere
                        int deltaK = iEndCol - iStartCol;
                        if (qpCurrPiece->GetPiece() == '\xC5' && (deltaK == -2 || deltaK == 2))
                            switch(deltaK) {
                                case 2:
                                    pieceOnSquare[iStartRow][5] = pieceOnSquare[iStartRow][7];
                                    pieceOnSquare[iStartRow][7] = 0;
                                    break;
                                case -2:
                                    pieceOnSquare[iStartRow][3] = pieceOnSquare[iStartRow][0];
                                    pieceOnSquare[iStartRow][0] = 0;
                                    break;
                            }
                    }
                    else { // Undo the last move
                        cout << "This move would put your King in Check." << endl;
                        pieceOnSquare[iStartRow][iStartCol] = pieceOnSquare[iEndRow][iEndCol];
                        pieceOnSquare[iEndRow][iEndCol]		= qpTemp;
                    }
                }
            }
        }
        if (!bValidMove) {
            cout << "Invalid Move!" << endl << endl;
        }
    } while (!bValidMove);
}

void CChess::AlternateTurn()
{
    playerTurn = (playerTurn == 'W') ? 'B' : 'W';
}

bool CChess::IsGameOver()
{
    // Check that the current player can move
    // If not, we have a stalemate or checkmate
    bool bCanMove(false);
    bCanMove = mqGameBoard.CanMove(playerTurn);
    if (!bCanMove) {
        if (mqGameBoard.IsInCheck(playerTurn)) {
            AlternateTurn();
            cout << "Checkmate, " << playerTurn << " Wins!" << endl;
        } else {
            cout << "Stalemate! " << endl;
        }
    }
    return !bCanMove;
}

void CChess::SaveGame(char playerTurn, CPiece* pieceOnSquare[8][8])
{
    string saveName;

    cout << "\n\tEnter save game name: ";   /// Bug when name includes spaces. Replace with getline.
    cin >> saveName;

    // To not deal with the huge ass headache that is Unicode characters in C++,
    // the pieces are converted to regular letters from extended ascii on export and back on import.
    ofstream savegame(saveName + ".sav");

    for (int r = 7; r >= 0; r--){
        for (int k = 0; k < 8; k++){
            if (pieceOnSquare[r][k] != 0){
                savegame << pieceOnSquare[r][k]->GetColor();
                char pieceAscii = pieceOnSquare[r][k]->GetPiece();
                switch (pieceAscii) {
                    case '\xEA':        // Pawn
                        savegame << "P";
                        break;
                    case '\xD2':        // Rook
                        savegame << "R";
                        break;
                    case '\x9C':        // Knight
                        savegame << "N";
                        break;
                    case '\xE8':        // Bishop
                        savegame << "B";
                        break;
                    case '\x9D':        // Queen
                        savegame << "Q";
                        break;
                    case '\xC5':        // King
                        savegame << "K";
                        break;
                }
            } else
                savegame << "00";       // Two zeros so it's easier to iterate thru in LoadGame().
        }
        //savegame << "\n";              // DO NOT put this back except to debug. It will break the loop in LoadGame() that reads the save file.
    }
    savegame << playerTurn;             // Saves which color has the first turn on load. Put at the end so it's easy to find.
    savegame << "ENDBOARD";             // To find where hasMoved info is in the file.

    // hasMoved info:
    for (int r = 7; r >= 0; r--){
        for (int k = 0; k < 8; k++){
                if (pieceOnSquare[r][k] != 0 && pieceOnSquare[r][k]->hasMoved == 1)     // Find pieces that have moved and save the state to file.
                    savegame << r << k;
        }
    }
    savegame << "ENDHASMOVED";           // To find where takenPieces info is in the file.

    // Taken pieces. Need to convert to regular letters again.
    for (int i = 0; i < 16; i++) {
        if (mqGameBoard.takenPiecesB[i] != 0) {
            savegame << mqGameBoard.takenPiecesB[i]->GetColor();
            char pieceAscii = mqGameBoard.takenPiecesB[i]->GetPiece();
            switch (pieceAscii) {
                case '\xEA':        // Pawn
                    savegame << "P";
                    break;
                case '\xD2':        // Rook
                    savegame << "R";
                    break;
                case '\x9C':        // Knight
                    savegame << "N";
                    break;
                case '\xE8':        // Bishop
                    savegame << "B";
                    break;
                case '\x9D':        // Queen
                    savegame << "Q";
                    break;
                case '\xC5':        // King
                    savegame << "K";
                    break;
            }
        }
        if (mqGameBoard.takenPiecesW[i] != 0) {
            savegame << mqGameBoard.takenPiecesW[i]->GetColor();
            char pieceAscii = mqGameBoard.takenPiecesW[i]->GetPiece();
            switch (pieceAscii) {
                case '\xEA':        // Pawn
                    savegame << "P";
                    break;
                case '\xD2':        // Rook
                    savegame << "R";
                    break;
                case '\x9C':        // Knight
                    savegame << "N";
                    break;
                case '\xE8':        // Bishop
                    savegame << "B";
                    break;
                case '\x9D':        // Queen
                    savegame << "Q";
                    break;
                case '\xC5':        // King
                    savegame << "K";
                    break;
            }
        }
    }
    savegame << "ENDTAKENPIECES";

    cout << "\n\tGame Saved! \n\n";
} // SaveGame()

void CChess::LoadGame()
{
    // Create new 8x8 CPiece* array to overwrite mqGameBoard.pieceOnSquare in StartLoadGame().
    bool wasFileFound = true;
    ifstream saveGame;

    do {
        wasFileFound = true;
        string saveName;
        cout << "\n\tEnter save game name: ";
        cin >> saveName;
        saveGame.open(saveName + ".sav");

        if (!saveGame) {
            cout << "\tERROR: File not found.\n";
            wasFileFound = false;
        }
    } while (!wasFileFound);

    stringstream buffer;
    buffer << saveGame.rdbuf();
    string saveStr = buffer.str();

    // Translate board info to a new 8x8 board.
    int saveBoardnpos = saveStr.find("ENDBOARD");                         // End pos in saveStr of board info.
    for (int i = 0, r = 7, k = 0; i < saveBoardnpos - 1; i += 2){         // saveBoardnpos - 1 to account for playerTurn char. i += 2 because each piece takes up 2 characters in the string. Empty squares are "00".
        if (k == 8) {                                                     // Loop condition makes sure last char (playerTurn) isn't included.
            r--;
            k = 0;
        }
        if (saveStr[i] == '0')
            loadPieceOnSquare[r][k] = 0;
        else if (saveStr[i] == 'B') {                  // Check for Black pieces.
            if (saveStr[i + 1] == 'P')
                loadPieceOnSquare[r][k] = new CPawn('B');
            if (saveStr[i + 1] == 'R')
                loadPieceOnSquare[r][k] = new CRook('B');
            if (saveStr[i + 1] == 'N')
                loadPieceOnSquare[r][k] = new CKnight('B');
            if (saveStr[i + 1] == 'B')
                loadPieceOnSquare[r][k] = new CBishop('B');
            if (saveStr[i + 1] == 'Q')
                loadPieceOnSquare[r][k] = new CQueen('B');
            if (saveStr[i + 1] == 'K')
                loadPieceOnSquare[r][k] = new CKing('B');
        }
        else if (saveStr[i] == 'W') {                  // Check for White pieces.
            if (saveStr[i + 1] == 'P')
                loadPieceOnSquare[r][k] = new CPawn('W');
            if (saveStr[i + 1] == 'R')
                loadPieceOnSquare[r][k] = new CRook('W');
            if (saveStr[i + 1] == 'N')
                loadPieceOnSquare[r][k] = new CKnight('W');
            if (saveStr[i + 1] == 'B')
                loadPieceOnSquare[r][k] = new CBishop('W');
            if (saveStr[i + 1] == 'Q')
                loadPieceOnSquare[r][k] = new CQueen('W');
            if (saveStr[i + 1] == 'K')
                loadPieceOnSquare[r][k] = new CKing('W');
        }
        k++;
    }
    // Overwrite playerTurn. When save file is exported playerTurn is the last character appended.
    playerTurn = saveStr[saveBoardnpos - 1];

    // Debug
//        for (int r = 7; r >= 0; r--){
//            for (int k = 0; k < 8; k++){
//                loadPieceOnSquare[r][k] != 0 ? cout << loadPieceOnSquare[r][k]->GetColor() << loadPieceOnSquare[r][k]->GetPiece() << " " : cout << "0  ";
//            }
//            cout << endl;
//        }

    // hasMoved info:
    int saveHasMovedStartPos = saveBoardnpos + 8;                                   // Start pos in saveStr of hasMoved info. +8 for the length of "ENDBOARD".
    int saveHasMovedNpos = saveStr.find("ENDHASMOVED");                             // End pos in saveStr of hasMoved info.
    for (int i = saveHasMovedStartPos, r, k; i < saveHasMovedNpos; i += 2) {        // Remember: Format in save file: "33" = pieceOnSquare[3][3]->hasMoved == 1. Empty file means no piece has moved.
        r = saveStr[i] - '0';                                                       // Convert string elements(char) to int.
        k = saveStr[i + 1] - '0';
        loadPieceOnSquare[r][k]->hasMoved = 1;                                      // Assign saved hasMoved value.
    }

    // Taken pieces:
    int saveTakenPiecesStartPos = saveHasMovedNpos + 11;                            // +11 for length of "ENDHASMOVED".
    int saveTakenPiecesNpos = saveStr.find("ENDTAKENPIECES");
    int bCount = 0, wCount = 0;                                                     // These are needed to make sure the taken pieces are stored starting from the beginning of the array.
    for (int i = saveTakenPiecesStartPos; i < saveTakenPiecesNpos; i++) {
        if (saveStr[i] == 'B') {
            if (saveStr[i + 1] == 'P')
                mqGameBoard.takenPiecesB[bCount] = new CPawn('B');
            if (saveStr[i + 1] == 'R')
                mqGameBoard.takenPiecesB[bCount] = new CRook('B');
            if (saveStr[i + 1] == 'N')
                mqGameBoard.takenPiecesB[bCount] = new CKnight('B');
            if (saveStr[i + 1] == 'B')
                mqGameBoard.takenPiecesB[bCount] = new CBishop('B');
            if (saveStr[i + 1] == 'Q')
                mqGameBoard.takenPiecesB[bCount] = new CQueen('B');
            bCount++;
        }
        if (saveStr[i] == 'W') {
            if (saveStr[i + 1] == 'P')
                mqGameBoard.takenPiecesW[wCount] = new CPawn('W');
            if (saveStr[i + 1] == 'R')
                mqGameBoard.takenPiecesW[wCount] = new CRook('W');
            if (saveStr[i + 1] == 'N')
                mqGameBoard.takenPiecesW[wCount] = new CKnight('W');
            if (saveStr[i + 1] == 'B')
                mqGameBoard.takenPiecesW[wCount] = new CBishop('W');
            if (saveStr[i + 1] == 'Q')
                mqGameBoard.takenPiecesW[wCount] = new CQueen('W');
            wCount++;
        }
    }
}

bool CChess::IsCommand(string input)
{
    for (unsigned int i = 0; i < input.length(); i++) {  // Returns false if input contains any digits.
        if(isdigit(input[i]))
            return false;
    }
    return true;
}


