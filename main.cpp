#include "ChessPieces.h"
#include "ChessBoardAndCChess.h"
//#include "ChessGame.h"

#include <cstdlib>
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <cctype>   // For isdigit().


// Things to add:
// 1. Instructions (specifically how to make special moves like Castling)
// 2. Pawn promotion.
// 3. String entry support (for save/load commands, letter columns).

// Bugs:
// 1. "Check" message.

// This only checks if the first char of the input in GetNextMove() is not a number.

int main()
{
//    Change console color scheme on Windows. Disabled for now.
//    HANDLE h = GetStdHandle ( STD_OUTPUT_HANDLE );
//    WORD wOldColorAttrs;
//    CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
//
//    SMALL_RECT windowRect = {0, 0, 72, 39};
//    SMALL_RECT* windowSize = &windowRect;       // Need this because third param in SetConsoleWindowInfo must be a SMALL_RECT*.
//    SetConsoleWindowInfo(h, TRUE, windowSize);
//    SetConsoleTitle("Chess v1 by Oleg P.");	// The L makes the string of type LPCWSTR.
//
//    //Save the current color information
//    GetConsoleScreenBufferInfo(h, &csbiInfo);
//    wOldColorAttrs = csbiInfo.wAttributes;
//
//    //Set the new color information
//    SetConsoleTextAttribute (h, 0xF0);

    CChess game;
    game.MainMenu();


//     Restore the original colors
//    SetConsoleTextAttribute (h, wOldColorAttrs);

    return 0;
}







