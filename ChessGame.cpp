#include "ChessGame.h"

CGame::CGame() {}

CGame::~CGame() {}

void CGame::MainMenu(bool wasExitCalled)
{
//    if (!wasExitCalled)
//        CChess* game;
//    if (wasExitCalled) {
//        delete game;
//        game = new CChess;
//    }

    int option;
    cout << "\t\tMAIN MENU\n\n"
         << "\t1. Start a new game. \n"
         << "\t2. Load a game: \n\n"
         << "Enter an option number: ";

    cin >> option;

    switch (option) {
        case 1:
        {
            CChess game;
            game.StartNewGame();
            break;
        }
        case 2:
        {
            CChess game;
            game.StartLoadGame();
            break;
        }
    }
}

