#include "ChessPieces.h"


// CPiece
CPiece::CPiece(char cColor) : pieceColor(cColor), hasMoved(0) {}
CPiece::~CPiece() {}

char CPiece::GetColor() { return pieceColor; }

bool CPiece::IsLegalMove(int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8]) // Checks if square is empty or occupied by opponents color.
{
    if ((gameboard[destR][destK] == 0) || (pieceColor != gameboard[destR][destK]->GetColor()))
        return IsDestAllowed (currentR, currentK, destR, destK, gameboard);
    return false;
}


// CPawn
CPawn::CPawn(char cColor) : CPiece(cColor) {}
CPawn::~CPawn() {}

char CPawn::GetPiece() { return '\xEA'; }

bool CPawn::IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8])
{
    if (gameboard[destR][destK] == 0) {                            // If destination square is empty.
        if (currentK == destK) {                                   // If destination is in the same column as the current column.
            if (GetColor() == 'W') {                               // If piece is white.
                if (destR == currentR + 1)                         // If destination is one row up.
                    return true;                                   // Move is allowed.
                else if (hasMoved == 0 && destR == currentR + 2 && // If piece has not moved yet AND destination is two rows up AND
                         gameboard[currentR + 1][currentK] == 0)   // square on row up is empty.
                    return true;
            }
            else {                                                 // If piece is black
                if (destR == currentR - 1)                         // If destination is one row down.
                    return true;                                   // Move is allowed.
                else if (hasMoved == 0 && destR == currentR - 2 && // If piece has not moved yet and destination is two rows down AND
                         gameboard[currentR - 1][currentK] == 0)   // square one row down is empty.
                    return true;
            }
        }
    }
    else {                                                         // Destination holds piece of opposite color
        if ((currentK == destK + 1) || (currentK == destK - 1)) {
            if (GetColor() == 'W') {
                if (destR == currentR + 1)
                    return true;
            }
            else {
                if (destR == currentR - 1)
                    return true;
            }
        }
    }
    return false;
}


// CRook
CRook::CRook(char cColor) : CPiece(cColor) {}
CRook::~CRook() {}

char CRook::GetPiece() { return '\xD2'; }

bool CRook::IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8])
{
    if (currentR == destR) {
        // Make sure that all invervening squares are empty
        int kOffset = (destK - currentK > 0) ? 1 : -1;
        for (int k = currentK + kOffset; k !=  destK; k += kOffset) {
            if (gameboard[currentR][k] != 0) {
                return false;
            }
        }
        return true;
    } else if (destK == currentK) {
        // Make sure that all invervening squares are empty
        int rOffset = (destR - currentR > 0) ? 1 : -1;
        for (int r = currentR + rOffset; r !=  destR; r += rOffset) {
            if (gameboard[r][currentK] != 0) {
                return false;
            }
        }
        return true;
    }
    return false;
}


// CKnight
CKnight::CKnight(char cColor) : CPiece(cColor) {}
CKnight::~CKnight() {}

char CKnight::GetPiece() { return '\x9C'; }

bool CKnight::IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8])
{
    // Destination square is unoccupied or occupied by opposite color
    if ((currentK == destK + 1) || (currentK == destK - 1)) {
        if ((currentR == destR + 2) || (currentR == destR - 2)) {
            return true;
        }
    }
    if ((currentK == destK + 2) || (currentK == destK - 2)) {
        if ((currentR == destR + 1) || (currentR == destR - 1)) {
            return true;
        }
    }
    return false;
}


// CBishop
CBishop::CBishop(char cColor) : CPiece(cColor) {}
CBishop::~CBishop() {}

char CBishop::GetPiece() { return '\xE8'; }

bool CBishop::IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8])
{
    if ((destK - currentK == destR - currentR) || (destK - currentK == currentR - destR)) {
        // Make sure that all invervening squares are empty
        int rOffset = (destR - currentR > 0) ? 1 : -1;
        int kOffset = (destK - currentK > 0) ? 1 : -1;
        int r;
        int k;
        for (r = currentR + rOffset, k = currentK + kOffset; r !=  destR; r += rOffset, k += kOffset) {
            if (gameboard[r][k] != 0)
                return false;
        }
        return true;
    }
    return false;
}


// CQueen
CQueen::CQueen(char cColor) : CPiece(cColor) {}
CQueen::~CQueen() {}

char CQueen::GetPiece() { return '\x9D'; }

bool CQueen::IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8])
{
    if (currentR == destR) {
        // Make sure that all invervening squares are empty
        int kOffset = (destK - currentK > 0) ? 1 : -1;
        for (int k = currentK + kOffset; k !=  destK; k = k + kOffset) {
            if (gameboard[currentR][k] != 0) {
                return false;
            }
        }
        return true;
    } else if (destK == currentK) {
        // Make sure that all invervening squares are empty
        int rOffset = (destR - currentR > 0) ? 1 : -1;
        for (int r = currentR + rOffset; r !=  destR; r = r + rOffset) {
            if (gameboard[r][currentK] != 0) {
                return false;
            }
        }
        return true;
    } else if ((destK - currentK == destR - currentR) || (destK - currentK == currentR - destR)) {
        // Make sure that all invervening squares are empty
        int rOffset = (destR - currentR > 0) ? 1 : -1;
        int kOffset = (destK - currentK > 0) ? 1 : -1;
        int r;
        int k;
        for (r = currentR + rOffset, k = currentK + kOffset;
            r !=  destR;
            r = r + rOffset, k = k + kOffset)
        {
            if (gameboard[r][k] != 0) {
                return false;
            }
        }
        return true;
    }
    return false;
}


// CKing
CKing::CKing(char cColor) : CPiece(cColor) {}
CKing::~CKing() {}

char CKing::GetPiece() { return '\xC5'; }

bool CKing::IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8])
{
    int iRowDelta = destR - currentR;
    int iColDelta = destK - currentK;
    if (((iRowDelta >= -1) && (iRowDelta <= 1)) &&
        ((iColDelta >= -1) && (iColDelta <= 1)))
        return true;
    // Castling:
    if (hasMoved == 0 &&                                                                                               // If King hasn't moved AND
       ((iColDelta == 2 && gameboard[currentR][currentK + 1] == 0 && gameboard[currentR][currentK + 2] == 0 &&         // Move is two squares to the right AND both squares to the right are empty AND
         gameboard[currentR][currentK + 3]->GetPiece() == '\xD2' && gameboard[currentR][currentK + 3]->hasMoved == 0)  // the piece three squares to the right is a Rook AND the Rook hasn't moved.
       ||                                                                                                              // OR
       (iColDelta == -2 && gameboard[currentR][currentK - 1] == 0 && gameboard[currentR][currentK - 2] == 0 &&         // Move is two squares to the left AND three squares to the left are empty AND
        gameboard[currentR][currentK - 3] == 0 && gameboard[currentR][currentK - 4]->GetPiece() == '\xD2' &&           // the piece four squares to the left is a Rook AND
        gameboard[currentR][currentK - 4]->hasMoved == 0)))                                                            // the Rook hasn't moved.
        {                                                                                                                       // NOTE: This block only checks the above situation.
        return true;                                                                                                            // Because CBoard::IsInCheck() needs to be called before a move is
        }                                                                                                                       // finalized, the Rook is moved in CChess::GetNextMove().
    else
        return false;
}






