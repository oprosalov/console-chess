# Console Chess

This is my first ever programming project, a console Chess game written in C++. Started with the tutorial at: http://xoax.net/cpp/crs/console/lessons/Lesson43/

The bulk of the work was done years ago during Spring break of my first C++ course, months before I had even learned what classes were. Hence, it is not exactly up to par, coding standards and efficiency-wise (to put it lightly), and the arithmetic is frightening to behold. I have too many projects in the works to allocate the time necessary to correct this, which would practically require starting from scratch.

Virtually all of the output code has been rewritten from the tutorial, and some major missing moves (such as initial two-square pawn move and castling) have been added. Also added a menu and game save/load. AI player is next on the todo list.