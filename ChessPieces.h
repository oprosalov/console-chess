#ifndef CHESSPIECES_H
#define CHESSPIECES_H


class CPiece
{
  private:
    char pieceColor;
    virtual bool IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8]) = 0; // Pure virtual function

  public:
    CPiece(char cColor);
    virtual ~CPiece();
    virtual char GetPiece() = 0;
    char GetColor();
    bool IsLegalMove(int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8]);
    bool hasMoved;
};


class CPawn : public CPiece
{
  private:
    virtual char GetPiece();
    bool IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8]);

  public:
    CPawn(char cColor);
    ~CPawn();
};


class CRook : public CPiece
{
  private:
    virtual char GetPiece();
    bool IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8]);

  public:
    CRook(char cColor);
    ~CRook();
};


class CKnight : public CPiece
{
  private:
    virtual char GetPiece();
    bool IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8]);

  public:
    CKnight(char cColor);
    ~CKnight();
};


class CBishop: public CPiece
{
  private:
    virtual char GetPiece();
    bool IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8]);

  public:
    CBishop(char cColor);
    ~CBishop();
};


class CQueen: public CPiece
{
  private:
    virtual char GetPiece();
    bool IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8]);

  public:
    CQueen(char cColor);
    ~CQueen();
};


class CKing: public CPiece
{
  private:
    virtual char GetPiece();
    bool IsDestAllowed (int currentR, int currentK, int destR, int destK, CPiece* gameboard[8][8]);

  public:
    CKing(char cColor);
    ~CKing();
};


#endif // CHESSPIECES_H
