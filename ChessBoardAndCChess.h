#ifndef CHESSBOARDANDCCHESS_H
#define CHESSBOARDANDCCHESS_H

//#include "ChessGame.h"
#include "ChessPieces.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
using namespace std;


class CBoard {
  public:
    CPiece* pieceOnSquare[8][8];
    CPiece* takenPiecesW[16];
    CPiece* takenPiecesB[16];

    CBoard();
    ~CBoard();
    void Print();
    bool IsInCheck(char pieceColor);
	bool CanMove(char pieceColor);

/** Can't figure out where to put this function. In the meantime, it is implemented in GetNextMove(). **/
//    // Note: All checks for Castling are done in CKing
//	// (empty spaces, if the Rook is there, and if either have moved yet).
//	// The only thing Castling() does is the movement of the Rook.
//	// King is moved like all other pieces (in CChess).
//
//	void Castling(int currentR, int deltaK) {
//        switch(deltaK) {
//            case 2:
//                pieceOnSquare[currentR][5] = pieceOnSquare[currentR][7];
//                pieceOnSquare[currentR][7] = 0;
//                break;
//            case -2:
//                pieceOnSquare[currentR][3] = pieceOnSquare[currentR][0];
//                pieceOnSquare[currentR][0] = 0;
//                break;
//        }
//	}
};

class CChess {
  private:
    CBoard mqGameBoard;
	CPiece* loadPieceOnSquare[8][8];
	char playerTurn;

  public:
    CChess();
	~CChess();
	void MainMenu(bool wasMenuCalled = false);
	void StartNewGame();
	void StartLoadGame();
	void ShowInstructions();
	void GetNextMove(CPiece* pieceOnSquare[8][8]);
	void AlternateTurn();
	bool IsGameOver();
	void SaveGame(char playerTurn, CPiece* pieceOnSquare[8][8]);
	void LoadGame();
	bool IsCommand(string);
};



#endif // CHESSBOARDANDCCHESS_H
